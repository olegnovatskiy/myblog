<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CategoryType;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 * @Route("category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     *
     * @Route("/", name="category_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();

        return $this->render('category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/create", name="category_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $form_category = $this->createForm(CategoryType::class);
        $form_category->handleRequest($request);

        if ($form_category->isSubmitted() && $form_category->isValid()) {
            $category = $form_category->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_view', ['slug' => $category->getSlug()]);
        }

        return $this->render('category/create.html.twig', array(
            'form_category' => $form_category->createView(),
        ));
    }

    /**
     * @Route("/{slug}", name="category_view")
     * @Method("GET")
     *
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        $comment_form = $this->createForm(CommentType::class, null, ['attr' =>[
            'class' => 'comment-form',
            'data-action' => $this->generateUrl('add_category_comment', ['slug' => $category->getSlug()])
        ]]);

        return $this->render('category/view.html.twig', array(
            'category' => $category,
            'comment_form' => $comment_form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Render template of post
     *
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderPost(Post $post)
    {
        return $this->render('partials/_post.html.twig', ['post' => $post]);
    }

    /**
     * Render template of comment
     *
     * @param Comment $comment
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderComment(Comment $comment)
    {
        return $this->render('partials/_comment.html.twig', ['comment' => $comment]);
    }

    /**
     * @Route("/comment/add/{slug}", name="add_category_comment")
     * @Method({"POST"})
     *
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addCommentAction(Request $request, Category $category)
    {
        if (!$category) {
            return $this->json(['status' => false, 'error' => "post not found"], Response::HTTP_BAD_REQUEST);
        }

        $comment_form = $this->createForm(CommentType::class);
        $comment_form->handleRequest($request);

        $comment = $comment_form->getData();
        $comment->setCategory($category);
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        $comment = $this->renderView('partials/_comment.html.twig', [
            'comment' => $comment
        ]);

        return $this->json([
            'status' => true,
            'comment' => $comment,
        ]);
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/edit/{slug}", name="category_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Category $category)
    {
        $delete_form = $this->createDeleteForm($category);
        $form_category = $this->createForm('AppBundle\Form\CategoryType', $category);
        $form_category->handleRequest($request);

        if ($form_category->isSubmitted() && $form_category->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/edit.html.twig', array(
            'form_category' => $form_category->createView(),
            'delete_form' => $delete_form->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            Category::remove($this->getDoctrine()->getManager(), $category);
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
