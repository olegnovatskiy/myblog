<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentType;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PostController
 * @package AppBundle\Controller
 * @Route("post")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="post_index")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository(Post::class)->findAll();

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/create", name="post_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $post_form = $this->createForm(PostType::class);
        $post_form->handleRequest($request);

        if ($post_form->isSubmitted() && $post_form->isValid()) {
            $post = $post_form->getData();

            if ($post->getFile()) {
                $post->uploadFile();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute("post_index");
        }

        return $this->render('post/create.html.twig', [
            'form_post' => $post_form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{slug}", name="post_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Post $post)
    {
        $file_path = $post->getFilePath();
        $delete_form = $this->createDeleteForm($post);
        $post_form = $this->createForm(PostType::class, $post);
        $post_form->handleRequest($request);

        if ($post_form->isSubmitted() && $post_form->isValid()) {
            $post = $post_form->getData();
            if ($post->getFile()) {
                $post->uploadFile($file_path);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('post_index');
        }


        return $this->render('post/edit.html.twig', [
            'form_post' => $post_form->createView(),
            'file_path' => $file_path,
            'delete_form' => $delete_form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}", name="post_view")
     * @Method({"GET"})
     *
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Post $post)
    {
        $delete_form = $this->createDeleteForm($post);

        $comment_form = $this->createForm(CommentType::class, null, ['attr' =>[
            'class' => 'comment-form',
            'data-action' => $this->generateUrl('add_post_comment', ['slug' => $post->getSlug()])
        ]]);

        return $this->render('post/view.html.twig', [
            'post' => $post,
            'comment_form' => $comment_form->createView(),
            'delete_form' => $delete_form->createView(),
        ]);
    }

    /**
     * @Route("/post/delete/{slug}", name="post_delete")
     * @Method({"DELETE"})
     *
     * @param Post $post
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            Post::remove($this->getDoctrine()->getManager(), $post);
        }
        return $this->redirectToRoute('post_index');
    }

    /**
     * @Route("/comment/add/{slug}", name="add_post_comment")
     * @Method({"POST"})
     *
     * @param Request $request
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addCommentAction(Request $request, Post $post)
    {
        if (!$post) {
            return $this->json(['status' => false, 'error' => "post not found"], Response::HTTP_BAD_REQUEST);
        }

        $comment_form = $this->createForm(CommentType::class);
        $comment_form->handleRequest($request);

        $comment = $comment_form->getData();
        $comment->setPost($post);
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        $comment = $this->renderView('partials/_comment.html.twig', [
            'comment' => $comment
        ]);

        return $this->json([
            'status' => true,
            'comment' => $comment,
        ]);
    }

    /**
     * Render template of comment
     *
     * @param Comment $comment
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderComment(Comment $comment)
    {
        return $this->render('partials/_comment.html.twig', ['comment' => $comment]);
    }

    /**
     * @param Post $post
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('post_delete', ['slug' => $post->getSlug()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
