<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Exception\UploadableException;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Exception;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 */
class Post
{
    const FILE_PATH = "img/uploads/post/";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={"image/png", "image/jpg", "image/img", "image/jpeg",},
     *     mimeTypesMessage="Please upload a file that has that extension: jpg, png, img or jpeg.",
     * )
     */
    private $file;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=true)
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post")
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Post
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Post
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get null
     *
     * @return string
     */
    public function getFile()
    {
        if (is_string($this->file)) {
            return null;
        } else {
            return $this->file;
        }
    }

    /**
     * Get path of exist file
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->file;
    }


    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('type', Comment::POST_TYPE));
        return $this->comments->matching($criteria);
    }

    /**
     * @param null $old_file_path
     * @return bool
     */
    public function uploadFile($old_file_path = null)
    {
        try {
            $file_name = uniqid('post_image_') . '.' . $this->file->guessExtension();
            $this->file->move(Post::FILE_PATH, $file_name);
            $this->file = Post::FILE_PATH . $file_name;

            if ($old_file_path != null && $old_file_path != "") {
                $filesystem = new Filesystem();
                $filesystem->remove($old_file_path);
            }

            return true;
        } catch (UploadableException $exception) {
            return false;
        }
    }

    public static function remove(ObjectManager $entityManager, Post $post, bool $with_flush = true)
    {
        $file_path = $post->getFilePath();
        try {
            $entityManager->remove($post);
            if ($with_flush) {
                $entityManager->flush();
                $filesystem = new Filesystem();
                $filesystem->remove($file_path);
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
