<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserSession
 *
 * @ORM\Table(name="user_session")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserSessionRepository")
 */
class UserSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=30)
     */
    private $session_id;

    /**
     * @var string
     *
     * @ORM\Column(name="browser", type="string", length=30)
     */
    private $browser;

    /**
     * @var string
     *
     * @ORM\Column(name="user_ip", type="string", length=25)
     */
    private $user_ip;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionId
     *
     * @param string $session_id
     *
     * @return UserSession
     */
    public function setSessionId($session_id)
    {
        $this->session_id = $session_id;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->session_id;
    }

    /**
     * Set browser
     *
     * @param string $browser
     *
     * @return UserSession
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;

        return $this;
    }

    /**
     * Get browser
     *
     * @return string
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * Set userIp
     *
     * @param string $user_ip
     *
     * @return UserSession
     */
    public function setUserIp($user_ip)
    {
        $this->user_ip = $user_ip;

        return $this;
    }

    /**
     * Get userIp
     *
     * @return string
     */
    public function getUserIp()
    {
        return $this->user_ip;
    }
}
