<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Exception;

class Visitors
{
    private $entity_manager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entity_manager = $entityManager;

        return $this;
    }

    /**
     * @return array
     */
    public function getVisitor()
    {
        $sql = "SELECT browser, COUNT(user_ip) AS user_count 
        FROM (SELECT DISTINCT browser, user_ip FROM user_session) AS res
        GROUP BY browser;";
        
        try {
            $query = $this->entity_manager->getConnection()->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $exception) {
            return [];
        }
    }
}
