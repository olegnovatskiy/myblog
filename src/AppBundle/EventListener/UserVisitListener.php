<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\UserSession;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UserVisitListener implements EventSubscriberInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            // constant that means kernel.request
            KernelEvents::REQUEST => 'onKernelRequest'
        );
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $session = $event->getRequest()->getSession();
        $session->start();
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $user_session = $this->entityManager
            ->getRepository(UserSession::class)
            ->findOneByIpSessionId($user_ip, $session->getId());

        if ($user_session) {
            return;
        }
        $user_session = new UserSession();
        $user_session->setUserIp($user_ip);
        $user_session->setSessionId($session->getId());
        $user_session->setBrowser($this->checkBrowserName());
        $this->entityManager->persist($user_session);
        $this->entityManager->flush();
    }

    private static function checkBrowserName()
    {
        $ExactBrowserNameUA=$_SERVER['HTTP_USER_AGENT'];

        if (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")) {
            // OPERA
            $ExactBrowserNameBR="Opera";
        } elseif (strpos(strtolower($ExactBrowserNameUA), "safari/")
            and strpos(strtolower($ExactBrowserNameUA), "chrome/")) {
            // CHROME
            $ExactBrowserNameBR="Chrome";
        } elseif (strpos(strtolower($ExactBrowserNameUA), "msie")) {
            // INTERNET EXPLORER
            $ExactBrowserNameBR="Internet Explorer";
        } elseif (strpos(strtolower($ExactBrowserNameUA), "firefox/")) {
            // FIREFOX
            $ExactBrowserNameBR="Firefox";
        } elseif (strpos(strtolower($ExactBrowserNameUA), "safari/")
            and strpos(strtolower($ExactBrowserNameUA), "opr/") == false
            and strpos(strtolower($ExactBrowserNameUA), "chrome/") == false) {
            // SAFARI
            $ExactBrowserNameBR="Safari";
        } else {
            // OUT OF DATA
            $ExactBrowserNameBR="OUT OF DATA";
        };

        return $ExactBrowserNameBR;
    }
}