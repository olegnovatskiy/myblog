$(document).ready(function () {
    $('body').on('submit', 'form[name="comment"]', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            method: "POST",
            url: $(this).data('action'),
            data: $(this).serialize(),
            type: 'json',
            success: function (data) {
                if (data.status) {
                    displayComment(data.comment);
                } else {
                    alert(data.error);
                }
            },
            error: function (data) {

            }
        });
        $('.comment-form').removeClass('comment-form-show');
        $('.comment-form').addClass('comment-form-hide');
        $('.add-comment').removeClass('plus-btn-hide');
        $('.add-comment').addClass('plus-btn-show');
    });

    function displayComment(comment)
    {
        $('.comments-list').append(comment)
        $('form[name="comment"] input').val('');
        $('form[name="comment"] textarea').val('');
    }

    $('body').on('click', '.add-comment', function (e) {
        e.preventDefault();
        $('.comment-form').removeClass('comment-form-hide');
        $('.comment-form').addClass('comment-form-show');
        $('.add-comment').removeClass('plus-btn-show');
        $('.add-comment').addClass('plus-btn-hide');
    })

});