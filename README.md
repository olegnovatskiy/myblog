My blog
====

git clone https://olegnovatskiy@bitbucket.org/olegnovatskiy/myblog.git

Instalation
----
    composer install

    in web folder - npm install

DB configuration
----
    php bin/console doctrine:database:create
    manually modify the charset and the collation of cretaed DB to  utf8 - utf8_unicode_ci
    php bin/console doctrine:migrations:migrate

Config apache virtual host 
-----------------
<VirtualHost *:80>
     DocumentRoot /path_to_project_folder/myblog/web
     ServerName myblog.local

     <Directory /path_to_project_folder/myblog/web>
             AllowOverride all
             Order Allow,Deny
             Allow from all
     </Directory>

     ErrorLog ${APACHE_LOG_DIR}/myblog_error.log
     CustomLog ${APACHE_LOG_DIR}/myblog_error.log combined
</VirtualHost>

.htaccess
--------
    - change web/.htaccess to your environment: app_dev.php or app.php